target/%.o: %.asm
		nasm -felf64 -o $@ $<

program: target/main.o target/dict.o target/lib.o
		ld -o $@ $^

.PHONY: clean
clean:
		rm target/*.o
		rm program
