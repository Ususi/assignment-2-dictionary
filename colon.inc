%define position 0

%macro colon 2
	%ifid %2
		%2: dq position
		%define position %2
	%else
		%error "Insert ID next time, please."
	%endif
	%ifstr %1
		db %1, 0
	%else
		%error "Insert string value to input line."
	%endif
%endmacro
